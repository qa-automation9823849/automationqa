package hw12;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Test {
    public static void main(String[] args) {

        User user1 = new User("John", "Dou", 22, "John@gmail.com");
        User user2 = new User("Jin", "Dou", 20, "Jina@gmail.com");
        User user3 = new User("Trinity", "Jonsonuk", 35, "John@gmail.com");
        User user4 = new User("Agent", "Smith", 28, "John@gmail.com");

        List<User> users = new ArrayList<>(List.of(user1, user2, user3, user4));

        System.out.println("Выводим результат задания 1:");
        users.stream()
                .sorted(Comparator.comparingInt(User::getAge))
                .map(User::getSurname)
                .distinct()
                .forEach(System.out::println);

//        Задание 2
        var userOne = users.stream()
                .filter(user -> user.getSurname().length() < 8)
                .map(User::getName)
                .findFirst()
                .orElseThrow(() -> new UserNotFoundException("Не нашли подходящую фамилию"));
        System.out.println("Выводим результат задания 2:");
        System.out.println(userOne);
    }
}


//   1. Создайте класс User, добавьте поля Name, Surname, Age, Email ( сеттеры, геттеры и т.д.), добавьте их в лист
//   и с помощью Stream, отфильтруйте их по возрасту и выведете только уникальные фамилии на экран.
//   (должны быть выведены только фамилии, без всего остального объекта).

//   2. Используя класс юзер, созданный ранее, отфильтруйте значения по длине фамилии (должно быть в списке только
//   фамилии менее 8 символов), далее вам нужно выставить так что бы были только имена вашего объекта и выведете первый
//   попавшийся результат, если результата нет - то должно выброситься исключение, вами созданное.

