package hw7;

                                        //Задание 1
public abstract class Animal {
    abstract void eat();

    abstract void drink();
}


interface Flyable {
    void fly();
}

interface Floating {
    void swim();
}

class Cat extends Animal implements Floating {

    @Override
    void eat() {
        System.out.println("Кот кушает");
    }

    @Override
    void drink() {
        System.out.println("Кот пьёт");
    }

    @Override
    public void swim() {
        System.out.println("Кот плавает");
    }

    void meows() {
        System.out.println("Кот мяукает");
    }
}

class ParrotAra extends Animal implements Flyable {

    @Override
    void eat() {
        System.out.println("Попугай ест");
    }

    @Override
    void drink() {
        System.out.println("Попугай пьет");
    }

    @Override
    public void fly() {
        System.out.println("Попугай летает");
    }
}

class Duck extends Animal implements Flyable, Floating {

    @Override
    void eat() {
        System.out.println("Утка ест");
    }

    @Override
    void drink() {
        System.out.println("Утка пьет");
    }

    @Override
    public void fly() {
        System.out.println("Утка летает");
    }

    @Override
    public void swim() {
        System.out.println("Утка плавает");
    }
}

class Test {
    public static void main(String[] args) {
        Cat cat = new Cat();
        cat.eat();
        cat.meows();
        Duck duck = new Duck();
        duck.fly();
    }
}



// Задание 1 :
// Создайте иерархию животных используя где логически необходимо абстрактные классы или интерфейсы, должно быть 3 класса
// животных ( любых) которые должны унаследовать базовые методы из класса родителя, далее напишите интерфейсы с
// какими-либо дополнительными методами, например - flyable(или аналогичные) и допишите к тем животным которым это подходит