package hw7;

                                            //Задание 2
public class A {
    void method() {
        System.out.println("Метод A");
    }
}
class B extends A {
    void method() {
        System.out.println("Метод В");
    }
    void methodA() {
        super.method();
    }
}
class C extends B {
    void method() {
        System.out.println("Метод С");
    }
    void methodB() {
        super.method();
    }
    void methodA() {
        super.methodA();
    }
}

class Main {
    public static void main(String[] args) {
        C c = new C();

        System.out.println();
        System.out.println("Здесь мы вызываем метод в классе С:");
        c.method();
        System.out.println();
        System.out.println("Здесь мы вызываем methodB в классе C, но конструкция super указывает на method " +
                "в классе В, поэтому выводим на экран method из класса В:");
        c.methodB();
        System.out.println();
        System.out.println("Здесь мы вызываем methodА в классе C, но в нём конструкция super указывает на " +
                "methodA в классе В,\n в котором его конструкция super указывает на method в классе А," +
                " поэтому выводим на экран method из класса А:");
        c.methodA();
    }
}

// Задание 2 :
// Воссоздайте систему multilevel наследования, на примере классов A <- B <- C, во всех классах перезапишите какой-то
// метод (любой), продемонстрируйте работу конструкции super. из класса С, объясните результат того что вывелось