package hw7;

                                        //Задание 3
public class Car {

    private Integer wheels;
    private String color;
    private String engine;

    public Integer getWheels() {
        return wheels;
    }

    public String getColor() {
        return color;
    }

    public String getEngine() {
        return engine;
    }

    public void setWheels(Integer wheels) {
        if (wheels < 0) {
            throw new RuntimeException("Incorrect count of wheels");
        }
        this.wheels = wheels;
    }

    public void setColor(String color) {
        if (!(color.equals("white"))) {
            throw new RuntimeException("Incorrect color");
        }
        this.color = color;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }
}


// Задание 3 :
// Создайте класс Car с какими либо параметрами (двигатель, цвет и т.п.), сделайте поля приватными и создайте
// сеттеры и геттеры, укажите какие-нибудь условия на сеттеры для создания объекта