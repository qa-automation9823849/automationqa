package hw6;

import java.util.Objects;

public class Car {

    private String body;
    private String color;
    private String engine;
    private String transmission;

    public String getBody() {
        return body;
    }

    public String getColor() {
        return color;
    }

    public String getEngine() {
        return engine;
    }

    public String getTransmission() {
        return transmission;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    @Override
    public String toString() {
        return "Car{" +
                "body='" + body + '\'' +
                ", color='" + color + '\'' +
                ", engine='" + engine + '\'' +
                ", transmission='" + transmission + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Objects.equals(body, car.body) && Objects.equals(color, car.color) && Objects.equals(engine, car.engine)
                && Objects.equals(transmission, car.transmission);
    }


////    Задание 2
    public boolean equals2(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Objects.equals(body, car.body);
    }

    public Car(String body, String color) {
        this.body = body;
        this.color = color;
    }

    public Car(String body, String color, String engine, String transmission) {
        this.body = body;
        this.color = color;
        this.engine = engine;
        this.transmission = transmission;
    }

    public static void main(String[] args) {

        Car car = new Car("sedan", "black", "1.8tsi", "auto");
        Car car2 = new Car("sedan", "black", "2.5GAS", "auto");

        System.out.println();
        System.out.println("Задание 1");
        System.out.println("Выводим сравнение двух объектов методом equals:");
        System.out.println(car.equals(car2));
        System.out.println("Задание 2");
        System.out.println("Выводим сравнение двух объектов методом equals2 только по 1 полю:");
        System.out.println(car.equals2(car2));
        System.out.println("Задание 3");
        System.out.println("Выводим объект класса:");
        System.out.println(car);
    }
}


//
//       1. Создать класс Car с какими либо приватными переменными, минимум 2 ( на ваш выбор) так что бы они логически
//               подходили, например "двигатель", "коробка передач" и т.д. Определить setters и getters
//               ( можно сгенерировать через идею,, но лучше написать самостоятельно) , метод toString и метод Equals,
//               далее создать 2 объекта и сравнить их.
//       2. Перепишите метод equals так, что бы сравнить объекты только по 1 полю, 2 активный метода equals
//               невозможно иметь в 1 классе, так что другой можете просто закомментировать
//       3. Выведете объект класса (с описанным в нем методом toString).
//               По желанию: его(метод toString) можно видоизменить что бы объект вывелся по другому формату.
