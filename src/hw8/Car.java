package hw8;

//        Задание 2.
//        Создайте класс Car или любой другой на ваш выбор, создайте геттеры и сеттеры и в сеттерах поставьте какие-либо условия
//        на инициализацию переменных или в конструкторе(  как делали на уроке) и в случае негативного кейса пробрасывайте
//        свое собственное исключение ( Создайте несколько своих исключений)


public class Car {
    private Integer wheels;
    private String color;

    public void setWheels(Integer wheels) {
        if (wheels < 4) {
            throw new WheelsCountException("Этот автомобиль должен иметь не менее 4 колеса");
        }
        if (wheels > 4) {
            throw new WheelsCountException("Этот автомобиль должен иметь не более 4 колеса");
        }
        this.wheels = wheels;
    }

    public void setColor(String color) {
        if (!(color.equals("black"))) {
            throw new BodyColorException("Этот автомобиль должен быть черного цвета");
        }
        this.color = color;
    }

    public Integer getWheels() {
        return wheels;
    }

    public String getColor() {
        return color;
    }

}

class WheelsCountException extends RuntimeException {
    public WheelsCountException(String message) {
        super(message);
    }
}

class BodyColorException extends RuntimeException {
    public BodyColorException(String message) {
        super(message);
    }
}

class TestExceptionsTypes {
    public static void main(String[] args) {
        Car car = new Car();
        car.setWheels(4);
        car.setColor("white");
    }
}
