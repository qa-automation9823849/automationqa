package hw8;
//        Задание 1.
//        Создайте enum Time, напишите перечисление типа: BREAKFAST, LANCH, DINNER и задайте им какое-то значение,
//        на ваш выбор, например - Time to breakfast, в методе main придумайте условие через if или switch и выведете на экран

public enum Time {
    BREAKFAST("Время ужинать"),
    LANCH("Время обедать"),
    DINNER("Время завтракать");

    private String value;

    Time(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}

class Period {

    Time periodTime;

    public Period(Time periodTime) {
        this.periodTime = periodTime;
    }
}

class Test {
    public static void main(String[] args) {
        Period period = new Period(Time.LANCH);

        switch (period.periodTime) {
            case BREAKFAST -> System.out.println(period.periodTime.getValue());
            case DINNER -> System.out.println(period.periodTime.getValue());
            case LANCH -> System.out.println(period.periodTime.getValue());
        }
    }
}