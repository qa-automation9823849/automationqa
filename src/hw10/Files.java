package hw10;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

// Задание 1
public class Files {
    public static void main(String[] args) throws IOException {
        String defaultFolder = "new";
        // Создание папки
        File directory = new File(defaultFolder, "directory");
        if (!directory.exists()) {
            if (directory.mkdir()) {
                System.out.println("Создали папку " + directory.getName());
            }
        } else {
            System.out.println("Папка " + directory.getName() + " уже создана");
        }

        // Создание двух файлов в папке
        File file1 = new File(directory, "file1.txt");
        File file2 = new File(directory, "file2.txt");
        if (file1.createNewFile() && file2.createNewFile()) {
            System.out.println("Создали файлы " + file1.getName() + "," + file2.getName());
        } else {
            System.out.println("Файлы " + file1.getName() + "," + file2.getName() + " уже созданы");
        }

        String text = "Java world";
        try(FileWriter writer = new FileWriter(file1);
            FileReader reader = new FileReader(file1);
            FileWriter writer2 = new FileWriter(file2)) {
            // Запись фразы в файл1
            writer.write(text);
            writer.flush();
            System.out.println("Фраза " + text + " успешно записана в файл: " + file1.getPath());

            // Чтение фразы из файла1 и запись в файл2
            int i;
            while ((i = reader.read()) != -1) {
                writer2.write((char) i);
            }
            writer2.flush();
            System.out.println("Фраза " + text + " успешно скопирована в файл: " + file2.getPath());

            //Удаляем файлы и папку
            file1.delete();
            file2.delete();
            directory.delete();

            //Проверяем наличие файлов после удаления
            if (directory.isDirectory()) {
                for (File item : directory.listFiles()) {
                    if (item.isDirectory()) {
                        System.out.println(item.getName() + " folder");
                    }
                    if (item.isFile()) {
                        System.out.println(item.getName() + " file");
                    }
                }
            } else {
                System.out.println("Файлы и папка удалены");
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}


//    Создать с помощью класса File папку ( проверить что она создана), в ней создать 2 файла
//    (так же проверить что они созданы), записать в 1 из файлов фразу "Java world". Далее используя FileWriter/Reader
//    или FileInput/OutputStream ( на ваш выбор), считать с 1-го файла фразу Java World и результат записать в
//    другой файл. После удалить оба файла используя класс File и удалить директорию.


