package hw10;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

// Задание 2
public class SerializationCat {
    public static void main(String[] args) {
        // Записываем объект в файл
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("cat.dat"))) {
            Cat cat = new Cat("Barsik", 3, 5.5);
            oos.writeObject(cat);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        // Считываем объект из файла
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("cat.dat"))) {
            Cat cat = (Cat) ois.readObject();
            System.out.println("Десериализация кота и чтение из файла:");
            System.out.println(cat);

        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}

//    Создайте объект Cat с полями - Name, Age, Weight, и сделать из него класс для сериализации, игнорируя поле Weight.
//    Далее запишите объект в файл и считайте его, выведя на экран. Используйте UID во избегания ошибок.
