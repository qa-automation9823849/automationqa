package hw3;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class HW3 {

    public static void main(String[] args) {


//                 Задание 1
        System.out.println("        Задание 1");
        System.out.println("    Введите размер массива");
        Scanner scanner = new Scanner(System.in);
        int[] arr1 = new int[scanner.nextInt()];

        for (int d = 0, i = 3; d < arr1.length; d++, i += 5) {
            arr1[d] = d * i;
        }
        System.out.println("    Получившийся результат:");
        System.out.println((Arrays.toString(arr1)));


        //                 Задание 3
        System.out.println();
        System.out.println("        Задание 3");
        int[][] arr = new int[2][5];
        System.out.println("    Выводим массив заполненный данными:");
        for (int i = 0; i < arr.length; i++) {
            System.out.print("[");
            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = new Random().nextInt(10);
                System.out.print(arr[i][j] + " ");
            }
            System.out.println("]");
        }
        System.out.println("    Выводим значения больше 2:");
        for (int i = 0; i < arr.length; i++) {
            System.out.print("[");
            for (int j = 0; j < arr[i].length; j++) {
                if (arr[i][j] > 2) {
                    System.out.print(arr[i][j] + " ");
                }
            }
            System.out.println("]");
        }

//        Задание 2
        System.out.println();
        System.out.println("        Задание 2");
        System.out.println("    Таблица умножения");
        for (int a = 9; a > 0; a--) {
            for (int b = 9; b > 0; b--) {
                System.out.println(" " + a + " * " + b + " = " + a * b);
            }
            System.out.println();
        }

    }
}


//    1. Создать массив integers, и с помощью класса scanner определить его размер, например
//            - int [] array = new int [ ' тут должно быть число со сканнера'], далее заполнить этот массив данными
//            (любыми, используя цикл for) и потом вывести все значения на экран, так же используя цикл for.
//    2. Используя вложенный цикл for, вывести на экран таблицу умножения только наоборот,
//            с начало умножение на 9 ( как делали на занятии).
//    3. Создайте двух мерный массив чисел, например int [] [] array = new [4][8] и заполните его данными
//            через вложенные циклы и выведите на экран их значения, если они больше 2, через другие вложенные циклы.
//            т.е. если массив хранит в себе числа 1, 2, 3, 4 - то на экран вывести только 3, 4.