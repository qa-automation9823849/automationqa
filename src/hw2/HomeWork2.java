package hw2;

import java.util.Scanner;

public class HomeWork2 {

    public static void main(String[] args) {

        int a = 222;
        short b = 12;
        long c = 3222L;
        float d = 1.2F;
        double e = 1.7;
        char f = '\u0000';
        boolean g = true;
        byte h = 4;

        System.out.println();
        System.out.println("            \"Задание 2 - \"Провести операции над ними, не только с одинаковыми типами," +
                " а и с разными (например - short + int) \"\n");

        System.out.println("Выводим значение а = " + a);
        System.out.println("Высчитываем суммму двух значений b+c = " + (b + c));
        System.out.println("Высчитываем суммму двух значений b+c деленное на h = " + (a + c) / h);
        System.out.println("Перед выводом значения е уменьшаем его на 1 = " + ++e);
        System.out.println("Выводим верность выражения (a > b или a < c) = " + (a < b || a > c));
        System.out.println("Выводим верность выражения (a > b и a > h) = " + (a > b && a > h));
        System.out.println();


        System.out.println("            \"Задание 3 - \"Повторить задание 2, но выполнить его в отдельных методах" +
                " и запустить эти методы в методе Main \"\n");

        System.out.println("Умножаем значение a на значение b = " + multiplicationNum(a, b));

        System.out.println("Выбираем минимальное значение между c и h = " + minNum((int) c, h));

        System.out.println("Делим значение с на значение а = " + divisionNum((int) c, a));

        System.out.println("Округляем значение е = " + roundNum((int) e));

        System.out.println();


        System.out.println("            \"Задание 4 - \"С помощью класса Scanner ввести целое число." +
                " Далее создайте условие, если это число четное то\n" +
                " выведите сообщение \"Четное число\", если нет то \"Не четное число\"\n");

        pair();
    }

    public static int multiplicationNum(int num1, int num2) {
        return num1 * num2;

    }

    public static int minNum(int num1, int num2) {
        return Math.min(num1, num2);

    }


    public static int divisionNum(int num1, int num2) {
        return (num1 / num2);

    }

    public static double roundNum(double num1) {
        return Math.round(num1);

    }



    public static void pair() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число для проверки на чётность: ");

        if (scanner.hasNextInt()) {
            int ab = scanner.nextInt();
            if (ab % 2 == 0) {
                System.out.println("Введенное число чётное");
            } else {
                System.out.println("Введенное число нечётное");
            }
        } else {
            System.out.println("Введено не целое число");

        }
    }
}

//       1  Создать переменные со всеми примитивными типами и инициализировать их значениями в методе Main
//       2  Провести операции над ними, не только с одинаковыми типами, а и с разными (например - short + int)
//       3  Повторить задание 2, но выполнить его в отдельных методах и запустить эти методы в методе Main
//       4  С помощью класса Scanner ввести целое число. Далее создайте условие, если это число четное то
//          выведите сообщение "Четное число", если нет то "Не четное число"


