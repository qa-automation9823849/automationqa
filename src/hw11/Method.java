package hw11;

public interface Method {
    int calculate(int x, int y);
}

class Lambda {
    public static void main(String[] args) {

        Method sum = (x, y) -> x + y;
        Method sumMin = (x, y) -> x - y;
        Method multiply = (x, y) -> x * y;
        Method divide = (x, y) -> x / y;

        System.out.println("Выводим сложение - " + sum.calculate(10, 3));
        System.out.println("Выводим вычитание - " + sumMin.calculate(10, 3));
        System.out.println("Выводим умножение - " + multiply.calculate(10, 3));
        System.out.println("Выводим деление - " + divide.calculate(10, 3));
    }
}

