package hw4;

public class HW4 {

    public static void main(String[] args) {

//        к заданию 2
        System.out.println();
        System.out.println("            \"Задание 2 - \"Написать метод который возвращает куб числа и вывести на экран." +
                "Можно использовать метод возведения числа в степень в библиотеке Math, \n" +
                " метод называется pow. Либо самостоятельно написать логику.\n");
        System.out.println("Результат: ");
        System.out.println(getQube(5));


//    к заданию 3
        System.out.println();
        System.out.println("            \"Задание 3 - \"Создайте массивы int[] со значениями [1,2,3,5,4,3,2,1,2,3,5]" +
                "далее нужно создать метод в который принимает некое число и массив, \n" +
                "суть в том что бы когда вызывался метод и подставлялась,  например цифра 2, " +
                "метод вел подсчет какое количество дубликата. Например counterCatch(array, 2) -> \n" +
                "должен вернуть количество повторений цифры 2 в массиве array. \n");

        int[] arr = new int[]{1, 2, 3, 5, 4, 3, 2, 1, 2, 3, 5};
        int[] arr2 = new int[]{1, 4, 8, 7, 9, 0, 3, 1, 2, 3, 7};
        int[] arr3 = new int[]{0, 2, 0, 5, 6, 3, 2, 1, 6, 3, 9};
        int[] arr4 = new int[]{8, 2, 0, 7, 4, 6, 0, 6, 5, 3, 5};

        System.out.println("Результат: ");
        System.out.println(counterCatch(arr3, 3));
    }


    //   Задание 1
    public static int getSum(int a) {
        return a;
    }

    public static int getSum(int a, int b) {
        return a + b;
    }

    public static int getSum(short d, int f) {
        return d * f;
    }

    public static int getSum(int a, int b, long c) {
        return (int) ((a + b) * c);
    }

    public static int getSum(int a, int b, boolean c, long d) {
        return c ? a + b : (int) (b + d);
    }


    //    Задание 2
    public static double getQube(double num) {
        return Math.pow(num, 3);
    }

    //    Задание 3
    public static int counterCatch(int[] arr, int duplicateValue) {
        int result = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == duplicateValue) {
                result = result + 1;
            }
        }
        return result;
    }

}

//    1. Напишите метод, например public static int getSum(int a) и перегрузите его 6 раз,
//       т.е. у вас должно получиться всего 6 методов с одинаковым названием,
//       но разным количеством атрибутов и разными типами данных.
//    2. Написать метод который возвращает куб числа и вывести на экран.
//       Можно использовать метод возведения числа в степень в библиотеке Math,
//       метод называется pow. Либо самостоятельно написать логику.
//    3. Создайте массивы int[] со значениями [1,2,3,5,4,3,2,1,2,3,5] - далее нужно создать метод
//       в который принимает некое число и массив, суть в том что бы когда вызывался метод и подставлялась,
//       например цифра 2, метод вел подсчет какое количество дубликата. Например counterCatch(array, 2) ->
//       должен вернуть количество повторений цифры 2 в массиве array.


