package hw5;

import static java.lang.String.format;

public class HW5 {

    public static void main(String[] args) {

//       Задание 1
        System.out.println();
        System.out.println("            \"Задание 1 - \"Создать строки \"Hello\" и \"World\" и объединить " +
                "  их всеми 3 способами конкатенации. \"\n");
        String s1 = "Hello";
        String s2 = "World";

        System.out.println("Результат:");
        System.out.println(s1 + " " + s2);
        System.out.println(s1.concat(" " + s2));
        System.out.println(format("%s %s", s1, s2));

//
//
//     Задание 2
        System.out.println();
        System.out.println("            \"Задание 2 - \"Найти среднее значение в строке \"Concatenation\" " +
                           " - результатом должно быть \\\"te\". \n" +
                           " Например для строки \"String\" - это будет \"ri\", \"Code\" - \"od\".\" \n");

        String s3 = "Concatenation";
        int middleIndex = s3.length() / 2;
        System.out.println(format("Для введенного слова - %s, результат будет : %s", s3,
                (s3.substring(middleIndex - 1, middleIndex + 1))));
//
//
//     Задание 3
        System.out.println();
        System.out.println("            \"Задание 3 - \"Составить список студентов используя метод format." +
                " Например - Студент [Имя] [Фамилия] [Факультет], \n" +
                "вместо значений в скобках подставить %s и соответствующее значение\" \n");

        String n = "Степан";
        String sn = "Степанов";
        String fac = "Механический";
        System.out.println("Результат:");
        System.out.println(format("Студент: %s %s, факультет-%s", n, sn, fac));

    }
}


//      1.  Создать строки "Hello" и "World" и объединить их всеми 3 способами конкатенации.
//
//      2.  Найти среднее значение в строке "Concatenation" - результатом должно быть "te".
//          Например для строки "String" - это будет "ri", "Code" - "od".
//
//      3.  Составить список студентов используя метод format. Например - Студент [Имя] [Фамилия] [Факультет],
//          вместо значений в скобках подставить %s и соответствующее значение
