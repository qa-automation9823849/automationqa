package hw9;

import java.util.ArrayList;
import java.util.List;

public class Test {

    public static void main(String[] args) {

        List<String> incomingList = new ArrayList<>();

        incomingList.add("Java");
        incomingList.add("Python");
        incomingList.add("Perl");
        incomingList.add("C++");
        incomingList.add("Java");
        incomingList.add("C++");
        incomingList.add("C#");

        System.out.println("Выводим первоначальный список:");
        System.out.println(incomingList);

        List<String> result = removeDuplicates(incomingList);

        System.out.println("Выводим список без дубликатов:");

        System.out.println(result);
    }

    public static List<String> removeDuplicates(List<String> dublicatesList) {

        List<String> outcomingList = new ArrayList<>();

        for (String str : dublicatesList) {
            if (!outcomingList.contains(str)) {
                outcomingList.add(str);
            }
        }
        return outcomingList;
    }
}

//Задание
//Создайте ArrayList, с элементами ["Java", "Python","Perl","C++","Java", "C++", C#"] -
//получите из коллекции новую коллекцию с уникальными значениями (можно использовать for для перебора)
